from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from .models import Categoria, Pelicula
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


def index(request):
    return render(request, 'app/home.html')


def form_registro_view(request):
    return render(request, 'app/registro.html')


def registro_post(request):
    # Obtiene los datos de registro
    nombres = request.POST['nombres']
    apellidos = request.POST['apellidos']
    username = request.POST['username']
    password = request.POST['password']

    # Crea un usuario
    usuario = User()
    usuario.first_name = nombres
    usuario.last_name = apellidos
    usuario.username = username
    usuario.set_password(password)

    print(usuario)
    # Guarda el usuario en la BD
    usuario.save()

    # Redireccionar a login
    return redirect('app:iniciar_sesion_view')


def iniciar_sesion_view(request):
    return render(request, 'app/login.html')


def iniciar_sesion_post(request):
    # Obtiene los datos de autenticación
    u = request.POST['username']
    p = request.POST['password']

    # Obtiene el usuario con username y password
    usuario = authenticate(username=u, password=p)

    if usuario is None:
        return render(request, 'app/errorLogin.html')
    else:
        # Inicia sesión
        login(request, usuario)
        return redirect('app:index')


def cerrar_sesion(request):
    # Cierra la sesión activa
    logout(request)
    return redirect('app:index')


@login_required
def categorias_view(request):
    # Obtiene todas las categorias
    lista = Categoria.objects.all()
    # Crea el contexto
    contexto = {
        'categorias': lista
    }

    return render(request, 'app/categorias.html', contexto)


@login_required
def categoria_view(request, id):
    # Crea el contexto
    contexto = {
        'actor': 'Tom Hanks',
        'edad': 64,
        'esJubilado': False,
        'filmografia': [
            {'titulo': 'Cast Away', 'year': 2000},
            {'titulo': 'Forrest Gump', 'year': 1994},
            {'titulo': 'DaVinci Code', 'year': 2006},
            {'titulo': 'Toy Story', 'year': 1995}
        ]
    }

    return render(request, 'app/categoria.html', contexto)


@login_required
def form_crear_categoria_view(request):
    return render(request, 'app/crearCategoria.html')


@login_required
def crear_categoria_post(request):
    # Obtiene los datos de la categoria
    nombre = request.POST['nombre']
    descripcion = request.POST['descripcion']

    # Crea el objeto Categoria
    c = Categoria()
    c.nombre = nombre
    c.descripcion = descripcion

    # Guarda la categoria en BD
    c.save()

    return redirect('app:categorias')


@login_required
def form_crear_pelicula_view(request):
    # Obtiene las categorias
    lista_categorias = Categoria.objects.all().order_by('nombre')

    # Crea el contexto
    contexto = {
        'categorias': lista_categorias
    }

    return render(request, 'app/crearPelicula.html', contexto)


@login_required
def crear_pelicula_post(request):
    # Obtiene los datos del formulario
    titulo = request.POST['titulo']
    year = int(request.POST['year'])
    sinopsis = request.POST['sinopsis']
    id_categoria = int(request.POST['categoria'])

    # Obtiene la categoria
    categoria = Categoria.objects.get(id=id_categoria)

    # Crea la pelicula
    p = Pelicula()
    p.titulo = titulo
    p.year = year
    p.sinopsis = sinopsis
    p.categoria = categoria

    # Guarda la pelicula en la BD
    p.save()

    return redirect('app:peliculas')


@login_required
def peliculas_view(request):
    # Obtiene las peliculas
    lista_peliculas = Pelicula.objects.order_by('titulo')

    contexto = {
        'peliculas': lista_peliculas
    }

    return render(request, 'app/peliculas.html', contexto)


def acerca_view(request):
    return render(request, 'app/acerca.html')
