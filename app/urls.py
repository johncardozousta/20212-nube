from django.urls import path
from .import views

app_name = 'app'
urlpatterns = [
    path('', views.index, name='index'),

    path('login/', views.iniciar_sesion_view, name='iniciar_sesion_view'),
    path('post_login/', views.iniciar_sesion_post, name='iniciar_sesion_post'),
    path('registro/', views.form_registro_view, name='form_registro_view'),
    path('registro_post/', views.registro_post, name='registro_post'),

    path('logout/', views.cerrar_sesion, name='cerrar_sesion'),

    # Lista de categorias
    path('categorias/', views.categorias_view, name='categorias'),
    # # Una categoria
    path('categorias/<int:id>', views.categoria_view, name='categoria'),
    # Formulario para crear categoría
    path('categorias/crear', views.form_crear_categoria_view,
         name="form_crear_categoria_view"),
    path('categorias/crear_post', views.crear_categoria_post,
         name="crear_categoria_post"),

    path('peliculas/', views.peliculas_view, name='peliculas'),
    # Formulario para crear película
    path('peliculas/crear', views.form_crear_pelicula_view,
         name="form_crear_pelicula_view"),
    path('peliculas/crear_post', views.crear_pelicula_post,
         name="crear_pelicula_post"),


    path('acerca/', views.acerca_view, name="acerca")
]
