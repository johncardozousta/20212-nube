# Proyecto AntiCorrupción

Este proyecto permite tener una plataforma en línea para que los ciudadanos puedan hacer verificación de los casos de corrupción en Colombia.

## Curso

Arquitectura de Soluciones en la Nube - 20212

## Herramientas

- Python
- Django
- Visual Studio Code
- HTML
- CSS
- Markdown
- AWS
